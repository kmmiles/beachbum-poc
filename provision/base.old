#!/bin/bash -e

##############################################################################
# cache
##############################################################################
#aptdir="/downloads/apt"
#if [ -d $(dirname "$aptdir") ]; then
#  sudo mkdir -p "$aptdir"
#  echo "dir::cache::archives $aptdir;" > /tmp/apt.conf
#  sudo mv /tmp/apt.conf /etc/apt/apt.conf
#fi

##############################################################################
# system
##############################################################################
set -eux; \
  sudo apt-get update && \
  sudo apt-get install -y --no-install-recommends \
    bash \
    sudo \
    curl \
    wget \
    vim \
    gcc \
    g++ \
    make \
    bc \
    git \
    screen \
    htop \
    pv \
    pwgen \
    gnupg \
    cmake \
    golang \
    cifs-utils \
    pulseaudio \
    cups \
    xterm \
    mate-desktop-environment-core \
    fonts-liberation \
    fonts-cros* \
    libappindicator3-1 \
    lsb-release \
    xdg-utils \
    apt-transport-https \
    ca-certificates \
    gnupg2 \
    software-properties-common

echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

##############################################################################
# docker
##############################################################################
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update && \
sudo apt-get -y install docker-ce
echo "aufs" > /tmp/aufs.conf
sudo mv /tmp/aufs.conf /etc/modules-load.d/aufs.conf
sudo modprobe aufs
cat << EOF > /tmp/daemon.json
{
  "storage-driver": "aufs"
}
EOF
sudo mv /tmp/daemon.json /etc/docker/daemon.json
sudo systemctl enable docker
sudo service docker restart
sudo usermod -aG docker vagrant

##############################################################################
# turbovnc
##############################################################################
(
  cd /tmp
  set -eux
  wget https://phoenixnap.dl.sourceforge.net/project/turbovnc/2.1.2/turbovnc_2.1.2_amd64.deb
  sudo dpkg -i turbovnc_2.1.2_amd64.deb
  sudo ln -sf /opt/TurboVNC/bin/* /usr/bin
)

##############################################################################
# vgl
##############################################################################
(
  cd /tmp
  set -eux
  wget https://phoenixnap.dl.sourceforge.net/project/virtualgl/2.5.2/virtualgl_2.5.2_amd64.deb
  sudo dpkg -i virtualgl_2.5.2_amd64.deb
)

##############################################################################
# nomachine
##############################################################################
#NOMACHINE_PACKAGE_NAME="nomachine_6.1.6_9_amd64.deb"
#NOMACHINE_MD5="00b7695404b798034f6a387cf62aba84"
#if [ ! -f nomachine.deb ]; then
#  curl -fSL "http://download.nomachine.com/download/6.1/Linux/${NOMACHINE_PACKAGE_NAME}" -o nomachine.deb
#fi
#echo "${NOMACHINE_MD5} *nomachine.deb" | md5sum -c - && \
#dpkg -i nomachine.deb && \
#rm -f nomachine.deb && \
#groupadd -r nomachine -g 433 && \
#useradd -u 431 -r -g nomachine -d /home/nomachine -s /bin/bash -c "NoMachine" nomachine && \
#mkdir /home/nomachine && \
#chown -R nomachine:nomachine /home/nomachine && \
#echo 'nomachine:nomachine' | chpasswd && \
#adduser nomachine sudo && \
#sudo usermod -aG docker nomachine

##############################################################################
# chrome
##############################################################################
(
  cd /tmp
  set -eux
  curl -fSL https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o chrome.deb && \
    sudo dpkg -i chrome.deb && \
    rm -f chrome.deb
)

##############################################################################
# vscode
##############################################################################
(
  cd /tmp
  set -eux
  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
  sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
  sudo apt-get update
  sudo apt-get install -y code 
)


##############################################################################
# dotfiles
##############################################################################
git clone https://gitlab.com/kmmiles/dotfiles.git
cd dotfiles && bin/install
[ -d /vagrant ] && sudo -H -u vagrant bin/install
[ -d /root ] && sudo bin/install
cd .. && rm -rf dotfiles
